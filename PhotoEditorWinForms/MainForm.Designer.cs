﻿namespace PhotoEditorWinForms
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.correctionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drawToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolPanel = new System.Windows.Forms.Panel();
            this.drawPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.colorLabel = new System.Windows.Forms.Label();
            this.brushSizeTrackBar = new System.Windows.Forms.TrackBar();
            this.colorPanel = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnSaveCopy = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.rotationPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.rotationTrackBar = new System.Windows.Forms.TrackBar();
            this.btnRotate = new System.Windows.Forms.Button();
            this.flipButton = new System.Windows.Forms.Button();
            this.btnCrop = new System.Windows.Forms.Button();
            this.correctionPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.lbBrightness = new System.Windows.Forms.Label();
            this.brightnessTrackBar = new System.Windows.Forms.TrackBar();
            this.lbContrast = new System.Windows.Forms.Label();
            this.contrastTrackBar = new System.Windows.Forms.TrackBar();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.menuStrip.SuspendLayout();
            this.toolPanel.SuspendLayout();
            this.drawPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.brushSizeTrackBar)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.rotationPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.rotationTrackBar)).BeginInit();
            this.correctionPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.brightnessTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.contrastTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.fileToolStripMenuItem, this.toolsToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1482, 28);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {this.openToolStripMenuItem, this.saveToolStripMenuItem, this.saveAsToolStripMenuItem, this.toolStripSeparator1, this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(57, 24);
            this.fileToolStripMenuItem.Text = "Файл";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(263, 24);
            this.openToolStripMenuItem.Text = "Открыть";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.OpenToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Enabled = false;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(263, 24);
            this.saveToolStripMenuItem.Text = "Сохранить";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Enabled = false;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys) (((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) | System.Windows.Forms.Keys.S)));
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(263, 24);
            this.saveAsToolStripMenuItem.Text = "Сохранить как..";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.SaveAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(260, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(263, 24);
            this.exitToolStripMenuItem.Text = "Выход";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {this.correctionToolStripMenuItem, this.rotationToolStripMenuItem, this.drawToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(115, 24);
            this.toolsToolStripMenuItem.Text = "Инструменты";
            // 
            // correctionToolStripMenuItem
            // 
            this.correctionToolStripMenuItem.Name = "correctionToolStripMenuItem";
            this.correctionToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D1)));
            this.correctionToolStripMenuItem.Size = new System.Drawing.Size(259, 24);
            this.correctionToolStripMenuItem.Text = "Корректировки";
            this.correctionToolStripMenuItem.Click += new System.EventHandler(this.CorrectionToolStripMenuItem_Click);
            // 
            // rotationToolStripMenuItem
            // 
            this.rotationToolStripMenuItem.Name = "rotationToolStripMenuItem";
            this.rotationToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D2)));
            this.rotationToolStripMenuItem.Size = new System.Drawing.Size(259, 24);
            this.rotationToolStripMenuItem.Text = "Обрезка и поворот";
            this.rotationToolStripMenuItem.Click += new System.EventHandler(this.RotationToolStripMenuItem_Click);
            // 
            // drawToolStripMenuItem
            // 
            this.drawToolStripMenuItem.Name = "drawToolStripMenuItem";
            this.drawToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D3)));
            this.drawToolStripMenuItem.Size = new System.Drawing.Size(259, 24);
            this.drawToolStripMenuItem.Text = "Рисование";
            this.drawToolStripMenuItem.Click += new System.EventHandler(this.DrawToolStripMenuItem_Click);
            // 
            // toolPanel
            // 
            this.toolPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.toolPanel.Controls.Add(this.drawPanel);
            this.toolPanel.Controls.Add(this.flowLayoutPanel1);
            this.toolPanel.Controls.Add(this.rotationPanel);
            this.toolPanel.Controls.Add(this.correctionPanel);
            this.toolPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.toolPanel.Location = new System.Drawing.Point(1161, 28);
            this.toolPanel.Name = "toolPanel";
            this.toolPanel.Size = new System.Drawing.Size(321, 825);
            this.toolPanel.TabIndex = 2;
            // 
            // drawPanel
            // 
            this.drawPanel.Controls.Add(this.colorLabel);
            this.drawPanel.Controls.Add(this.brushSizeTrackBar);
            this.drawPanel.Controls.Add(this.colorPanel);
            this.drawPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.drawPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.drawPanel.Location = new System.Drawing.Point(0, 448);
            this.drawPanel.Name = "drawPanel";
            this.drawPanel.Padding = new System.Windows.Forms.Padding(7, 28, 7, 14);
            this.drawPanel.Size = new System.Drawing.Size(321, 271);
            this.drawPanel.TabIndex = 9;
            this.drawPanel.Visible = false;
            // 
            // colorLabel
            // 
            this.colorLabel.Font = new System.Drawing.Font("Segoe UI", 10.2F);
            this.colorLabel.Location = new System.Drawing.Point(10, 28);
            this.colorLabel.Name = "colorLabel";
            this.colorLabel.Size = new System.Drawing.Size(299, 23);
            this.colorLabel.TabIndex = 2;
            this.colorLabel.Text = "Цвет";
            this.colorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // brushSizeTrackBar
            // 
            this.brushSizeTrackBar.Location = new System.Drawing.Point(10, 54);
            this.brushSizeTrackBar.Maximum = 100;
            this.brushSizeTrackBar.Minimum = 1;
            this.brushSizeTrackBar.Name = "brushSizeTrackBar";
            this.brushSizeTrackBar.Size = new System.Drawing.Size(306, 56);
            this.brushSizeTrackBar.TabIndex = 0;
            this.brushSizeTrackBar.TickFrequency = 10;
            this.brushSizeTrackBar.Value = 10;
            this.brushSizeTrackBar.Scroll += new System.EventHandler(this.BrushSizeTrackBar_Scroll);
            // 
            // colorPanel
            // 
            this.colorPanel.BackColor = System.Drawing.Color.Black;
            this.colorPanel.Location = new System.Drawing.Point(10, 116);
            this.colorPanel.Name = "colorPanel";
            this.colorPanel.Size = new System.Drawing.Size(299, 10);
            this.colorPanel.TabIndex = 1;
            this.colorPanel.Click += new System.EventHandler(this.ColorPanel_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnSaveCopy);
            this.flowLayoutPanel1.Controls.Add(this.btnReset);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 725);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(7, 14, 7, 14);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(321, 100);
            this.flowLayoutPanel1.TabIndex = 8;
            // 
            // btnSaveCopy
            // 
            this.btnSaveCopy.Font = new System.Drawing.Font("Segoe UI Semilight", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.btnSaveCopy.Location = new System.Drawing.Point(10, 28);
            this.btnSaveCopy.Name = "btnSaveCopy";
            this.btnSaveCopy.Size = new System.Drawing.Size(147, 41);
            this.btnSaveCopy.TabIndex = 0;
            this.btnSaveCopy.Text = "Сохранить";
            this.btnSaveCopy.UseVisualStyleBackColor = true;
            this.btnSaveCopy.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Segoe UI Semilight", 12F);
            this.btnReset.Location = new System.Drawing.Point(163, 28);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(146, 41);
            this.btnReset.TabIndex = 1;
            this.btnReset.Text = "Сбросить";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.BtnReset_Click);
            // 
            // rotationPanel
            // 
            this.rotationPanel.Controls.Add(this.label1);
            this.rotationPanel.Controls.Add(this.rotationTrackBar);
            this.rotationPanel.Controls.Add(this.btnRotate);
            this.rotationPanel.Controls.Add(this.flipButton);
            this.rotationPanel.Controls.Add(this.btnCrop);
            this.rotationPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.rotationPanel.Location = new System.Drawing.Point(0, 214);
            this.rotationPanel.Name = "rotationPanel";
            this.rotationPanel.Padding = new System.Windows.Forms.Padding(7, 28, 7, 14);
            this.rotationPanel.Size = new System.Drawing.Size(321, 234);
            this.rotationPanel.TabIndex = 7;
            this.rotationPanel.Visible = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.label1.Location = new System.Drawing.Point(10, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(299, 23);
            this.label1.TabIndex = 8;
            this.label1.Text = "Выравнивание";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rotationTrackBar
            // 
            this.rotationTrackBar.Location = new System.Drawing.Point(10, 54);
            this.rotationTrackBar.Maximum = 45;
            this.rotationTrackBar.Minimum = -45;
            this.rotationTrackBar.Name = "rotationTrackBar";
            this.rotationTrackBar.Size = new System.Drawing.Size(299, 56);
            this.rotationTrackBar.TabIndex = 7;
            this.rotationTrackBar.TickFrequency = 9;
            this.rotationTrackBar.Scroll += new System.EventHandler(this.RotationTrackBar_Scroll);
            // 
            // btnRotate
            // 
            this.btnRotate.Font = new System.Drawing.Font("Segoe UI Semilight", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.btnRotate.Location = new System.Drawing.Point(10, 116);
            this.btnRotate.Name = "btnRotate";
            this.btnRotate.Size = new System.Drawing.Size(147, 41);
            this.btnRotate.TabIndex = 4;
            this.btnRotate.Text = "Повернуть";
            this.btnRotate.UseVisualStyleBackColor = true;
            this.btnRotate.Click += new System.EventHandler(this.BtnRotate_Click);
            // 
            // flipButton
            // 
            this.flipButton.Font = new System.Drawing.Font("Segoe UI Semilight", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.flipButton.Location = new System.Drawing.Point(163, 116);
            this.flipButton.Name = "flipButton";
            this.flipButton.Size = new System.Drawing.Size(146, 41);
            this.flipButton.TabIndex = 6;
            this.flipButton.Text = "Перевернуть";
            this.flipButton.UseVisualStyleBackColor = true;
            this.flipButton.Click += new System.EventHandler(this.FlipButton_Click);
            // 
            // btnCrop
            // 
            this.btnCrop.Font = new System.Drawing.Font("Segoe UI Semilight", 12F);
            this.btnCrop.Location = new System.Drawing.Point(10, 163);
            this.btnCrop.Name = "btnCrop";
            this.btnCrop.Size = new System.Drawing.Size(299, 41);
            this.btnCrop.TabIndex = 9;
            this.btnCrop.Text = "Обрезать";
            this.btnCrop.UseVisualStyleBackColor = true;
            // 
            // correctionPanel
            // 
            this.correctionPanel.Controls.Add(this.lbBrightness);
            this.correctionPanel.Controls.Add(this.brightnessTrackBar);
            this.correctionPanel.Controls.Add(this.lbContrast);
            this.correctionPanel.Controls.Add(this.contrastTrackBar);
            this.correctionPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.correctionPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.correctionPanel.Location = new System.Drawing.Point(0, 0);
            this.correctionPanel.Name = "correctionPanel";
            this.correctionPanel.Padding = new System.Windows.Forms.Padding(7, 28, 7, 14);
            this.correctionPanel.Size = new System.Drawing.Size(321, 214);
            this.correctionPanel.TabIndex = 5;
            // 
            // lbBrightness
            // 
            this.lbBrightness.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBrightness.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.lbBrightness.Location = new System.Drawing.Point(10, 28);
            this.lbBrightness.Name = "lbBrightness";
            this.lbBrightness.Size = new System.Drawing.Size(299, 23);
            this.lbBrightness.TabIndex = 1;
            this.lbBrightness.Text = "Яркость";
            this.lbBrightness.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // brightnessTrackBar
            // 
            this.brightnessTrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.brightnessTrackBar.Location = new System.Drawing.Point(10, 54);
            this.brightnessTrackBar.Maximum = 100;
            this.brightnessTrackBar.Minimum = -100;
            this.brightnessTrackBar.Name = "brightnessTrackBar";
            this.brightnessTrackBar.Size = new System.Drawing.Size(299, 56);
            this.brightnessTrackBar.TabIndex = 0;
            this.brightnessTrackBar.TickFrequency = 20;
            this.brightnessTrackBar.Scroll += new System.EventHandler(this.BrightnessContrastTrackBar_Scroll);
            // 
            // lbContrast
            // 
            this.lbContrast.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbContrast.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.lbContrast.Location = new System.Drawing.Point(10, 113);
            this.lbContrast.Name = "lbContrast";
            this.lbContrast.Size = new System.Drawing.Size(299, 23);
            this.lbContrast.TabIndex = 2;
            this.lbContrast.Text = "Контрастность";
            this.lbContrast.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // contrastTrackBar
            // 
            this.contrastTrackBar.Location = new System.Drawing.Point(10, 139);
            this.contrastTrackBar.Maximum = 200;
            this.contrastTrackBar.Name = "contrastTrackBar";
            this.contrastTrackBar.Size = new System.Drawing.Size(299, 56);
            this.contrastTrackBar.TabIndex = 3;
            this.contrastTrackBar.TickFrequency = 20;
            this.contrastTrackBar.Value = 100;
            this.contrastTrackBar.Scroll += new System.EventHandler(this.BrightnessContrastTrackBar_Scroll);
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Location = new System.Drawing.Point(0, 28);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(1161, 825);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 3;
            this.pictureBox.TabStop = false;
            this.pictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PictureBox_MouseDown);
            this.pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PictureBox_MouseMove);
            this.pictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PictureBox_MouseUp);
            this.pictureBox.Resize += new System.EventHandler(this.PictureBox_Resize);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Файлы изображений|*.bmp;*.png;*.jpg";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "Jpg файл|*.jpg|Bmp файл|*.bmp|Png файл|*.png|Все файлы|*.*";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1482, 853);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.toolPanel);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Фоторедактор";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.toolPanel.ResumeLayout(false);
            this.drawPanel.ResumeLayout(false);
            this.drawPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.brushSizeTrackBar)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.rotationPanel.ResumeLayout(false);
            this.rotationPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.rotationTrackBar)).EndInit();
            this.correctionPanel.ResumeLayout(false);
            this.correctionPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.brightnessTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.contrastTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem correctionToolStripMenuItem;
        private System.Windows.Forms.Panel toolPanel;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.TrackBar brightnessTrackBar;
        private System.Windows.Forms.Label lbBrightness;
        private System.Windows.Forms.TrackBar contrastTrackBar;
        private System.Windows.Forms.Label lbContrast;
        private System.Windows.Forms.Button btnRotate;
        private System.Windows.Forms.FlowLayoutPanel correctionPanel;
        private System.Windows.Forms.Button flipButton;
        private System.Windows.Forms.FlowLayoutPanel rotationPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar rotationTrackBar;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnSaveCopy;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnCrop;
        private System.Windows.Forms.ToolStripMenuItem drawToolStripMenuItem;
        private System.Windows.Forms.FlowLayoutPanel drawPanel;
        private System.Windows.Forms.TrackBar brushSizeTrackBar;
        private System.Windows.Forms.Panel colorPanel;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Label colorLabel;
    }
}

