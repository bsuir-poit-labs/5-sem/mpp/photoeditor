﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PhotoEditorWinForms
{
    public partial class MainForm : Form
    {
        private PhotoEditor _photoEditor;

        private bool _isDrawing;
        private float _zoom = 1f;
        private RectangleF _imageArea = RectangleF.Empty;

        private Color _brushColor = Color.Black;

        public MainForm()
        {
            InitializeComponent();
        }

        private void BrightnessContrastTrackBar_Scroll(object sender, EventArgs e)
        {
            if (_photoEditor == null)
                return;

            pictureBox.Image = _photoEditor.AdjustImage(
                brightnessTrackBar.Value, contrastTrackBar.Value,
                rotationTrackBar.Value, false, false);
        }

        private void RotationTrackBar_Scroll(object sender, EventArgs e)
        {
            if (_photoEditor == null)
                return;

            pictureBox.Image = _photoEditor.AdjustImage(
                brightnessTrackBar.Value, contrastTrackBar.Value,
                rotationTrackBar.Value, false, false);
        }

        private void BtnRotate_Click(object sender, EventArgs e)
        {
            if (_photoEditor == null)
                return;

            pictureBox.Image = _photoEditor.AdjustImage(
                brightnessTrackBar.Value, contrastTrackBar.Value,
                rotationTrackBar.Value, true, false);
        }

        private void FlipButton_Click(object sender, EventArgs e)
        {
            if (_photoEditor == null)
                return;

            pictureBox.Image = _photoEditor.AdjustImage(
                brightnessTrackBar.Value, contrastTrackBar.Value,
                rotationTrackBar.Value, false, true);
        }

        private void BtnReset_Click(object sender, EventArgs e)
        {
            if (_photoEditor == null)
                return;

            pictureBox.Image = _photoEditor.Reset();
            pictureBox.Invalidate();
            SetDefaultValues();
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() != DialogResult.OK)
                return;

            try
            {
                Image image = Image.FromFile(openFileDialog.FileName);
                _photoEditor = new PhotoEditor(image, openFileDialog.FileName);
            }
            catch (OutOfMemoryException)
            {
                MessageBox.Show(@"Ошибка чтения картинки!");
                return;
            }

            pictureBox.Image = _photoEditor.AdjustedImage;
            SetImageScale();

            SetDefaultValues();
            saveAsToolStripMenuItem.Enabled = true;
            saveToolStripMenuItem.Enabled = true;
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_photoEditor == null)
                return;

            if (saveFileDialog.ShowDialog() != DialogResult.OK)
                return;

            _photoEditor.SaveImage(saveFileDialog.FileName);
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (_photoEditor == null)
                return;

            if (String.IsNullOrEmpty(_photoEditor.ImagePath))
            {
                SaveAsToolStripMenuItem_Click(sender, e);
                return;
            }

            _photoEditor.SaveImage();
        }

        private void RotationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rotationPanel.Visible = true;
            correctionPanel.Visible = false;
            drawPanel.Visible = false;
        }

        private void CorrectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            correctionPanel.Visible = true;
            rotationPanel.Visible = false;
            drawPanel.Visible = false;
        }

        private void DrawToolStripMenuItem_Click(object sender, EventArgs e)
        {
            correctionPanel.Visible = false;
            rotationPanel.Visible = false;
            drawPanel.Visible = true;
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void PictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            _isDrawing = true;
            ScalePoint(e.Location);
        }

        private void PictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (pictureBox.Image == null)
                return;

            if (_isDrawing)
            {
                using (var graphics = Graphics.FromImage(_photoEditor.AdjustedImage))
                {
                    var currentPoint = ScalePoint(e.Location);
                    var size = brushSizeTrackBar.Value;
                    var brush = new SolidBrush(_brushColor);

                    graphics.DrawEllipse(new Pen(_brushColor, 1), currentPoint.X - (size / 2),
                        currentPoint.Y - (size / 2), size, size);
                    graphics.FillEllipse(brush, currentPoint.X - (size / 2), 
                        currentPoint.Y - (size / 2), size, size);
                }

                pictureBox.Invalidate();
            }
        }

        private void PictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            _isDrawing = false;
        }

        private void PictureBox_Resize(object sender, EventArgs e)
        {
            SetImageScale();
        }

        private Point ScalePoint(Point point)
        {
            return new Point(
                (int) ((point.X - _imageArea.X) / _zoom),
                (int) ((point.Y - _imageArea.Y) / _zoom)
            );
        }

        private void SetImageScale()
        {
            if (_photoEditor == null)
                return;

            SizeF pictureBoxSize = pictureBox.ClientSize;
            SizeF imageSize = _photoEditor.AdjustedImage.Size;
            float pictureBoxRatios = 1f * pictureBoxSize.Width / pictureBoxSize.Height;
            float imageRatios = 1f * imageSize.Width / imageSize.Height;

            if (pictureBoxRatios > imageRatios)
            {
                _zoom = pictureBoxSize.Height / imageSize.Height;
                float width = imageSize.Width * _zoom;
                float left = (pictureBoxSize.Width - width) / 2;
                _imageArea = new RectangleF(left, 0, width, pictureBoxSize.Height);
            }
            else
            {
                _zoom = pictureBoxSize.Width / imageSize.Width;
                float height = imageSize.Height * _zoom;
                float top = (pictureBoxSize.Height - height) / 2;
                _imageArea = new RectangleF(0, top, pictureBoxSize.Width, height);
            }
        }

        private void SetDefaultValues()
        {
            brightnessTrackBar.Value = 0;
            contrastTrackBar.Value = 100;
            rotationTrackBar.Value = 0;
        }

        private void BrushSizeTrackBar_Scroll(object sender, EventArgs e)
        {
            colorPanel.Height = brushSizeTrackBar.Value;
        }

        private void ColorPanel_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() != DialogResult.OK)
                return;

            colorPanel.BackColor = _brushColor = colorDialog.Color;
        }
    }
}